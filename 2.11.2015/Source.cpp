// 1
// 1 2
// 1 2 3
// ....
// 1 2 3 ... n

// serii de numere
#include <iostream>
using namespace std;

int main() {
	for (int i = 1;i <= 9;i++) {
		for (int j = 1;j <= 9 - i;j++) {
			cout << " ";
		}
		for (int j = 1;j <= i;j++) {
			cout << j << " ";
		}
		cout << endl;
	}
}

